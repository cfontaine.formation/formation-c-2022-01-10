#include <iostream>
#include "Point.h"
using namespace std;

int main()
{
    Point a(1, 2);
    Point b(3, 1);

    Point inv = -a;
    inv.afficher();

    Point c = a + b;
    c.afficher();

    c = a * 4;
    c.afficher();
    // c = 4 * a;
    cout << (a == b) << endl;

    c=++a;
    c.afficher();

    c = a++;
    c.afficher();
    a.afficher();

    cout << a[0] << " " << a[1] << endl;
    cout << ((a + b)*4 )<< endl;
}
