#pragma once

//class Test;

//class Other
//{
//public:
//	void traitement(Test &t);
//
//};

class Test
{
	int val;

public:
	Test(int val) : val(val) {	}

	int getVal() const
	{
		return val;
	}

	void afficher() const;

	// Fonction amie
	friend void fonctionAmie(Test& t);
	friend void fonctionAmie2();
	//friend void Other::traitement(Test& t);

	// Classe Amie
	friend class ClasseAmie;
};

//void Other::traitement(Test &t)
//{
//	t.val = 111;
//}
