#include <iostream>
#include "Test.h"
#include "ClasseAmie.h"


// fonctionAmie peut acc�der � la variable d'instance val de la classe Test car elle est d�clar� ammie
void fonctionAmie(Test& t) {
    t.val = 123;
}

Test t2(4);

void fonctionAmie2() {
    t2.val = 345;
}


int main()
{
    // Fonction amie
    Test t(1);
    t.afficher();
    fonctionAmie(t);
    t.afficher();
    
    t2.afficher();
    fonctionAmie2();
    t2.afficher();

    // Classe amie
    ClasseAmie ca(t);
    t.afficher();
    ca.traitement();
    t.afficher();
}
