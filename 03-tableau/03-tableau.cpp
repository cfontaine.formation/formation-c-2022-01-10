#include <iostream>
//#include <climits>

using namespace std;

// en C++ 98
enum Direction
{
	NORD = 90, SUD = 270, OUEST = 180, EST = 0
};

enum Direction2
{
	N = 90, S = 270, O = 180, E = 0
};

// en C++11
enum class Motorisation
{
	ESSENCE,
	GPL,
	ELECTRIQUE,
	DIESEL,
	HYDROGENE
};

enum class DirectionE
{
	NORD, OUEST, SUD, EST
};

int main()
{
	// tableau � une dimension
	// D�claration: type nom[taille]
	int t[5];

	// Initialisation du tableau
	for (int i = 0; i < 5; i++)
	{
		t[i] = 0;
	}

	// Acc�s � un �l�ment du tableau
	t[0] = 3;
	cout << t[0] << endl;

	int t2[5] = {}; // Initialisation du tableau � 0

	for (int i = 0; i < 5; i++)
	{
		cout << t2[i] << endl;
	}

	// Parcourir un tableau en C++11
	// Pas de modification des �l�ments du tableau
	for (auto v : t)
	{
		cout << v << endl;
	}

	//   En utilisant une r�f�rence, on peut  modifier des �l�ments du tableau
	for (auto& v : t)
	{
		cout << v << endl;
		v = 10;
	}
	for (auto v : t)
	{
		cout << v << endl;
	}

	// Calculer la taille d'un tableau (ne fonctionne pas avec les tableaux pass�s en param�tre de fonction)
	cout << "nombre d'�l�ment tableau=" << sizeof(t) / sizeof(t[0]) << endl;

	// Exercice : tableau
	// Trouver la valeur maximale et la moyenne d�un tableau de 5 entiers: -7, 4, 8, 0, -3
	int tab[] = { -7, -4, -8, -9, -3 };
	int max = INT_MIN;//tab[0];
	double somme = 0.0;
	for (auto elm : tab)
	{
		if (elm > max)
		{
			max = elm;
		}
		somme += elm;
	}
	cout << "Maximum=" << max << " Moyenne=" << somme / 5 << endl;

	// Tableau � 2 dimensions
	  // D�claration
	double t2d[3][2] = {}; // + initilialisation � 0 
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 2; j++) {
			cout << t2d[i][j] << '\t';
		}
		cout << endl;
	}

	// Acc�s � un �l�ment
	t2d[0][1] = 3.4;
	cout << t2d[0][1] << endl;
	cout << t2d[1] << "  " << &t2d[1][0] << endl;

	// Calculer le nombre  de colonnne  d'un tableau (ne fonctionne pas avec les tableaux pass�s en param�tre de fonction)
	int nbColone = sizeof(t2d[0]) / sizeof(t2d[0][0]);
	cout << "NbColonne=" << nbColone << endl;
	// Calculer le nombre  de ligne  d'un tableau
	int nbLigne = sizeof(t2d) / sizeof(t2d[0]);
	cout << "NbLigne=" << nbLigne << endl;
	// Calculer le nombre  d'�l�ment  d'un tableau
	int nbElem = sizeof(t2d) / sizeof(t2d[0][0]);
	cout << "NbElement=" << nbElem << endl;

	// Pointeur
	double val = 12.34;

	// D�claration d'un pointeur de double
	double* ptr;

	// &val => adresse de la variable val
	cout << val << "adresse=" << &val << endl;

	ptr = &val;
	cout << ptr << endl;

	// *ptr => Acc�s au contenu de la variable pointer par ptr
	cout << *ptr << endl;
	*ptr = 123.45;
	cout << val << endl;

	// Un pointeur qui ne pointe sur rien
	double* ptr2 = 0;       // 0 en C++ (correspond � NULL en C)
	double* ptr3 = nullptr; // nullptr en C++11

	//  On peut affecter un pointeur avec un pointeur du m�me type
	ptr2 = ptr; // ptr2 contient aussi l'adresse de val
	cout << ptr2 << "   " << *ptr2 << endl;

	// On peut comparer 2 pointeurs du m�me type
	if (ptr == ptr2) {
		cout << "pointeurs  �gaux" << endl;
	}

	// ou comparer un pointeur � 0 ou nullptr
	if (!ptr3) { //ptr3==nullptr ou ptr3==0
		cout << "le pointeur  est null" << endl;
	}

	// Pointeur constant
	// En pr�fixant un pointeur avec const => Le contenu de la variable point�e est constant
	const double* ptrCst = &val;
	cout << ptrCst << " " << *ptrCst << endl;
	//*ptrCst = 5.67;   // on ne peut plus modifier le contenu point� par l'interm�diaire du pointeur
	//ptrCst = nullptr;   // mais on peut modifier le pointeur

	 // En postfixant un pointeur avec const => le pointeur est constant
	double* const ptrCtrPost = &val;
	cout << ptrCtrPost << " " << ptrCtrPost << endl;
	*ptrCtrPost = 678.90; // On peut modofier le contenu point�
	//ptrCtrPost = ptr;   // Le pointeur ptrCst2  est constant, on ne peut plus le modifier

	// En pr�fixant et en postfixant un pointeur avec const: le pointeur ptrCst2 est constant et
	// l'on ne peut plus modifier le contenu de la variable point�e par l'interm�diaire du pointeur
	const double* const ptrCst2 = &val;
	cout << ptrCst2 << " " << *ptrCst2 << endl;
	//*ptrCst2 = 12.345;
	//ptrCst2 = ptr;

	// Op�rateur de cast -> supprimer le qualificatif const
	double* ptr4 = (double*)ptrCst;	// en C (int*)ptrCst
	*ptr4 = 3456.9;

	double* ptr5 = const_cast<double*>(ptrCst);	// en C++, const_cast permet de supprimer le qualificatif const
	*ptr5 = 1111111.222;

	// Exercice Pointeur
	double v1 = 3, v2 = 4, v3 = 10;
	double* ptrChoix = 0;

	int choix;
	cin >> choix;
	switch (choix)
	{
	case 1:
		ptrChoix = &v1;
		break;
	case 2:
		ptrChoix = &v2;
		break;
	case 3:
		ptrChoix = &v3;
		break;
	}
	if (ptrChoix != 0)
	{
		cout << *ptrChoix << endl;
	}

	// Tableau et Pointeur
	double tp[] = { 1.2,3.4,5.7, 4.8 };
	cout << tp << endl;	// Le nom d'un tableau est un pointeur sur le prem�re �l�ment
	cout << *tp << endl;	// On acc�de au premier �l�ment du tableau

	// On acc�de au 3�me �l�ment du tableau
	cout << tp + 2 << endl;  // �quivalent � tp + sizeof(int)*2

	cout << *(tp + 2) << endl; // �quivalent � t2[2]

	double* ptrTp = tp;
	cout << *(ptrTp + 2) << endl;
	cout << ptrTp[2] << endl;

	// Arithm�tique des pointeurs
	cout << *ptrTp << endl;		// 1.2
	ptrTp++;					// ptrTp=ptrTp+1
	cout << *ptrTp << endl;		// 3.4	
	ptrTp++;
	cout << *ptrTp << endl;		// 5.7
	cout << ptrTp - tp << endl;	//2

	// Pointeur void -> C
	void* ptrVoid = ptrTp;
	double* ptrTp3 = (double*)ptrVoid;
	cout << *ptrTp3 << endl;

	int* ptrTp4 = (int*)ptrVoid;
	cout << *ptrTp4 << endl;
	ptrTp4++;
	cout << *ptrTp4 << endl;

	// reinterpret_cast
	int rti = 0x61 | 0x64 << 8 | 0X50 << 16; // 00 50 64 61;
	cout << hex << rti << endl;
	int* ptrRti = &rti;
	char* ptrC = reinterpret_cast<char*>(ptrRti);
	cout << static_cast<int>(*ptrC) << endl;	// 61
	cout << static_cast<int>(*(ptrC + 1)) << endl;			// 64
	cout << static_cast<int>(*(ptrC + 2)) << endl;			// 50
	cout << static_cast<int>(ptrC[0]) << " " << ptrC[0] << endl;
	cout << static_cast<int>(ptrC[1]) << " " << ptrC[1] << dec << endl;

	// Allocation dynamique de m�moire
	double* ptrDyn = new double;	// new => allocation de la m�moire
	*ptrDyn = 12.34;
	cout << ptrDyn << " " << *ptrDyn << endl;
	delete ptrDyn;		 // delete => lib�ration de la m�moire
	ptrDyn = nullptr; //0


	// Allocation dynamique avec initialisation
	ptrDyn = new double(4.56);
	delete ptrDyn;

	ptrDyn = new double{ 4.56 }; // C++11;
	cout << *ptrDyn << endl;
	delete ptrDyn;

	// Allocation dynamique d'un tableau
	int s = 5;
	char* ptrTabDyn = new char[s];	// new [] => cr�ation d'un tableau dynamique
	*ptrTabDyn = 'a';			// ptrTabDyn[0]='a';
	*(ptrTabDyn + 1) = 'b';		// ptrTabDyn[1]='b';
	// On peut acc�der � un tableau dynamique comme � un tableau statique
	ptrTabDyn[2] = 'c';
	ptrTabDyn[3] = 'd';
	ptrTabDyn[4] = 'e';

	for (int i = 0; i < 5; i++)
	{
		cout << ptrTabDyn[i] << endl;
	}
	delete[] ptrTabDyn;	// delete [] => lib�ration d'un tableau dynamique
	ptrTabDyn = nullptr;

	// Exercice : Tableau dynamique
	// Modifier le programme Tableau pour faire la saisie :
	// - de la taille du tableau
	// - des �l�ments du tableau
	// Trouver la valeur maximale et la moyenne du tableau

	int size;
	cin >> size;
	if (size > 0)
	{
		int* ptrT = new int[size];

		for (int i = 0; i < size; i++)
		{
			cout << "t[" << i << "]=";
			cin >> ptrT[i];
		}
		int maximum = ptrT[0];
		double somme2 = ptrT[0];
		for (int i = 1; i < size; i++)
		{
			if (ptrT[i] > maximum)
			{
				maximum = ptrT[i];
			}
			somme2 += ptrT[i];
		}
		cout << "Maximum=" << maximum << " Moyenne=" << somme2 / size << endl;
		delete[] ptrT;
	}

	// R�f�rence 
	// Une r�f�rence correspond � un autre nom que l'on donne � la variable
	// On doit aussi intialiser une r�f�rence avec une lvalue (= qui poss�de une adresse)
	int b = 789;
	int& ref = b;
	ref = 3;
	cout << ref << " " << b << endl;
	// int& ref2 = 56;	// Erreur pas d'initialisation

	// R�f�rence constante
	const int& refCst = b;
	cout << refCst << endl;
	// refCst = s;	// On ne peut plus utiliser une r�f�rence constante pour modifier la valeur

	// On peut initialiser une r�f�rence constante avec une valeur lit�rale
	const int& refCstL = 123;
	cout << refCstL << endl;

	// Enum�ration
	Direction dir = Direction::NORD; // NORD
	int vDir = dir; // Convertion implicite enum -> int

	cout << dir << " " << vDir << endl;
	// Eviter la conversion int => enum
	//vDir = 120;		
	//dir = (Direction)vDir;
	cout << dir << endl;

	// probl�me avec les enums, il n'y pas de v�rification de type
	dir = NORD;
	Direction2 dir2 = Direction2::N; //N
	if (dir == dir2)	// compare les valeurs num�rique
	{
		cout << " egal" << endl;  // dir et dir2 sont consid�r� comme �gal m�me si ceux sont des enum�rations diff�rente
	}

	// enum class C++11
	Motorisation m = Motorisation::DIESEL;
	// avec enum class, il n'y a plus de convertion implicite enum -> int
	int vm = static_cast<int>(m);	// il faut utiliser un static_cast
	cout << vm << endl;

	DirectionE dire = DirectionE::NORD;

	//if (m == dire) {		// Erreur -> avec enum class il y a une v�rification de type 
	//	cout << " egal" << endl;
	//}

	switch (dire) {
	case DirectionE::NORD:
		cout << "NORD" << endl;
		break;
	case DirectionE::EST:
		cout << "SUD" << endl;
		break;
	default:
		cout << "Autre" << endl;
	}
}
