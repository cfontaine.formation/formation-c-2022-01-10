#pragma once
#include "Animal.h"
class Refuge
{
	Animal* animaux[10];
	int placeOccuper;

public:
	Refuge() : placeOccuper(0)
	{
		for (int i = 0; i < 10; i++) {
			animaux[i] = nullptr;
		}
	}
	void ajouter(Animal* a);
	void ecouter();
};

