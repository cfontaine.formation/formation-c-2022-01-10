#pragma once
#include "Animal.h"
#include <string>

class Chien : public Animal
{
	std::string nom;

public:
	Chien(int age, double poid , const std::string &nom) : Animal(age,poid), nom(nom) {}

	std::string getNom() const
	{
		return nom;
	}


	void emmetreSon() override final;	// avec virtual le type de retour doit �tre le m�me
										// override (C++11) v�rification du compilateur que c'est bien une red�finition
										// final (C++11) interdire pour les sous-classes une red�finition "virtuelle" 
	int afficher();  // sans virtual la m�thode red�finie elle peut avoir un type de retour diff�rent
};

