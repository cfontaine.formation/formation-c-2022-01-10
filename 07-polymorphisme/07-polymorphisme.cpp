#include "Animal.h"
#include "Chien.h"
#include "Chat.h"
#include "Refuge.h"
#include <iostream>
#include <typeinfo>
using namespace std;

int main()
{
	//Animal* a1 = new Animal(5, 5000);
	//a1->afficher();
	//a1->emmetreSon();

	Chien ch1(5, 4500, "Laika");
	ch1.emmetreSon();
	cout << ch1.getNom() << endl;

	Animal* a2 = new Chien(3, 7000, "Rolo");
	a2->emmetreSon(); // virtual appel de la méthode emmetreSon de Chien
	a2->afficher(); //  sans virtual appel de la méthode afficher de Animal

	Animal* a3 = &ch1;
	a3->emmetreSon();

	//Chien* ch2 = (Chien*)a2;
	Chien* ch2 = dynamic_cast<Chien*>(a2);
	if (ch2) { // ch2!=nullptr
		ch2->emmetreSon();
		cout << ch2->getNom();
	}

	//Chien* ch3 = dynamic_cast<Chien*>(a1); // retourner 0 ou nullptr (c++11) et exception bad_cast
	//if (ch3) {
	//	ch3->emmetreSon();
	//	cout << ch3->getNom() << endl;
	//}
	
	// typeid retourne un objet de type: type_info
	cout << typeid(*a2).name() << endl; //Chien
//	cout << typeid(*a1).name() << endl; //Animal
	//if (typeid(*a2) != typeid(*a1))
	//{
	//	cout << "type different" << endl;
	//}

//	delete a1;
	delete a2;

	Refuge r;
	Chien chien1( 11, 3000, "Idefix");
	Chien* chien2= new Chien(3, 5000, "Rolo");
	Chat chat1(5, 3500);
	r.ajouter(&chien1);
	r.ajouter(chien2);
	r.ajouter(&chat1);
	r.ecouter();
	delete chien2;
}

