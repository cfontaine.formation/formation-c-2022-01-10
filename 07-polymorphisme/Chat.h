#pragma once
#include "Animal.h"
class Chat :
    public Animal
{
    int nbVie;
public:
    Chat(int age,double poid) : Animal(age,poid),nbVie(9) {}

    void emmetreSon();

    int getNbVie() const
    {
        return nbVie;
    }
};

