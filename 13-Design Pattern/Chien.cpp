#include "Chien.h"
#include <iostream>
using namespace std;

void Chien::emmetreSon()
{
	cout << nom << " aboie" << endl;
}

void Chien::afficher()
{
	cout << nom << endl;
	Animal::afficher();
}

Animal* Chien::clone()
{
	return new Chien(*this);
}
