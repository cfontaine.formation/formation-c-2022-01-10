#pragma once
#include "Animal.h"
#include <map>
#include<string>

class FactoryAnimal2
{

    static std::map<std::string, Animal*> m;

public:

    FactoryAnimal2() = default;
    static void enregister(const std::string& typeAnimal, Animal* animal);
    Animal* getAnimal(std::string typeAnimal);
};