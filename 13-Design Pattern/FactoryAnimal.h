#pragma once
#include "Animal.h"
#include<string>

enum class TypeAnimal { CHIEN, CHAT };

class FactoryAnimal
{

public:
	Animal* getAnimal(TypeAnimal);
};