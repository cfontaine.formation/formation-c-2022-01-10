#pragma once
#include "Animal.h"
class Chat :
	public Animal
{
	int nbVie;
public:
	Chat() : Chat(0,0){ }
	Chat(int age, double poid) : Animal(age, poid), nbVie(9) {}

	void emmetreSon();

	int getNbVie() const
	{
		return nbVie;
	}

	void diminuerNbVie();
	Animal* clone() override;
};

