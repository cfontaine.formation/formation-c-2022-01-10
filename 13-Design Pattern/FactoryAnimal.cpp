#include "FactoryAnimal.h"
#include "Chien.h"
#include "Chat.h"


Animal* FactoryAnimal::getAnimal(TypeAnimal type)
{
    switch(type)
    {
        case TypeAnimal::CHIEN:
            return new Chien();
        break;
        default:
            return new Chat();
        break;
    }
}
