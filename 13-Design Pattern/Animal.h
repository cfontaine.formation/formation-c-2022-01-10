#pragma once
class Animal
{
	int age;
	double poid;

public :
	Animal(int age, double poid) : age(age), poid(poid) { }

	void afficher(); 

	virtual void emmetreSon() =0;
	virtual  Animal* clone() = 0;
};

