#pragma once
#include "Animal.h"
#include <string>

class Chien : public Animal
{
	std::string nom;

public:
	Chien() : Chien(0, 0, "") {}
	Chien(int age, double poid, const std::string& nom) : Animal(age, poid), nom(nom) {}

	std::string getNom() const
	{
		return nom;
	}

	void emmetreSon() override;
	Animal* clone() override;
	void afficher();
};

