#include <iostream>
#include "Singleton.h"
#include "FactoryAnimal.h"
#include "FactoryAnimal2.h"
#include "Chien.h"
#include "Chat.h"

using namespace std;

int main()
{
	Singleton* s = Singleton::getInstance();
	s->setValeur(100);
	std::cout << s->getValeur() << std::endl;
	Singleton* s1 = Singleton::getInstance();
	std::cout << s1->getValeur() << std::endl;

	FactoryAnimal f;
	Animal* a = f.getAnimal(TypeAnimal::CHAT);
	a->emmetreSon();

	FactoryAnimal2 f2;
	FactoryAnimal2::enregister("chat", new Chat());
	FactoryAnimal2::enregister("chien", new Chien());
	Animal* a1 = f2.getAnimal("chien");
	a1->emmetreSon();

	return 0;
}