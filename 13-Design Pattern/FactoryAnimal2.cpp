#include "FactoryAnimal2.h"
#include "Chien.h"
#include "Chat.h"

std::map<std::string, Animal*> FactoryAnimal2::m = std::map<std::string, Animal*>();
void FactoryAnimal2::enregister(const std::string& typeAnimal, Animal* animal)
{
    if (m.find(typeAnimal) == m.end()) {
        m[typeAnimal] = animal;
    }
}


Animal* FactoryAnimal2::getAnimal(std::string typeAnimal)
{
    auto it = m.find(typeAnimal);
    if (it != m.end())
    {
        return (*it).second->clone();
    }
    return nullptr;
}