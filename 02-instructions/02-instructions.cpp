#include <iostream>
using namespace std;

int main()
{
	// Moyenne
	// Saisir 2 nombres entiers et afficher la moyenne dans la console
	int a;
	int b;
	cin >> a >> b;
	double res = (a + b) / 2.0;
	cout << "Moyenne=" << res << endl;

	//  Exercice : Nombre Pair
	// Créer un programme qui indique, si le nombre entier saisie dans la console est paire ou impaire
	int va;
	cin >> va;
	if (!(va & 1)) //va % 2 == 0
	{
		cout << "Paire" << endl;
	}
	else
	{
		cout << " Impaire" << endl;
	}

	// Exercice : Intervalle
	// Saisir un nombre et dire s'il fait parti de l'intervalle -4 (exclus) et 7 (inclus)
	int val;
	cin >> val;
	if (val > -4 && val <= 7)
	{
		cout << val << " fait parti de l'interval" << endl;
	}

	// Switch
	const int LUNDI = 1;
	int jour;
	cin >> jour;
	switch (jour)
	{
	case LUNDI:
		cout << "Lundi" << endl;
		break;
	case LUNDI + 5:
	case LUNDI + 6:
		cout << "Week-end" << endl;
		break;
	default:
		cout << "Autre jour" << endl;
	}

	// C++17 déclaration dans un if
	int v1, v2;
	cin >> v1 >> v2;
	if (int n = 1; v1 < v2) {
		cout << ++n << endl;
	}
	else {
		cout << --n << endl;
	}
	// n = 0;

	// Instructions de branchement inconditionnel
	for (int i = 0; i < 10; i++) {
		if (i == 3) {
			break;	// break => termine la boucle
		}
		cout << i << endl;
	}

	for (int i = 0; i < 10; i++) {
		if (i == 3) {
			continue; // continue => on passe à l'itération suivante
		}
		cout << i << endl;
	}

	// goto
	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 3; j++) {
			if (i == 3) {
				goto EXIT_LOOP;	// Utilisation de goto pour sortir de 2 boucles imbriquées
			}
			cout << i << " " << j << endl;
		}
	}
EXIT_LOOP:

	// Faire un programme calculatrice
	//	Saisir dans la console
	//	- un double
	//	- une caractère opérateur qui a pour valeur valide : + - * /
	//	- un double

	// Afficher:
	// - Le résultat de l’opération
	// - Une message d’erreur si l’opérateur est incorrecte
	// - Une message d’erreur si l’on fait une division par 0
	char op;
	double val1, val2;
	cin >> val1 >> op >> val2;
	switch (op) {
	case '+':
		cout << val1 << " + " << val2 << " = " << (val1 + val2) << endl;
		break;
	case '-':
		cout << val1 << " - " << val2 << " = " << (val1 - val2) << endl;
		break;
	case '*':
		cout << val1 << " * " << val2 << " = " << (val1 * val2) << endl;
		break;
	case '/':
		if (!val2)
		{
			cout << "Division par 0" << endl;
		}
		else
		{
			cout << val1 << " - " << val2 << " = " << (val1 / val2) << endl;
		}
		break;
	default:
		cout << "L'opérateur " << op << " est incorrecte" << endl;
	}

	// Exercice : Table de multiplication
	// Faire un programme qui affiche la table de multiplication pour un nombre entre 1 et 9
	//	1 X 4 = 4
	//	2 X 4 = 8
	//	…
	//	9 x 4 = 36
	//	Si le nombre passé en paramètre est en dehors de l’intervalle 1 à 9, on arrête sinon on redemande une nouvelle valeur
	for (;;) // while(true)  => boucle infinie
	{
		int vm;
		cin >> vm;
		if (vm < 1 || vm > 9)
		{
			break;
		}
		for (int i = 1; i < 9; i++)
		{
			cout << i << " x " << vm << " = " << (i * vm) << endl;
		}
	}

	// Exercice : Quadrillage
	// Créer un quadrillage dynamiquement on saisit le nombre de colonne et le nombre de ligne
	//	ex : pour 2 3
	//	[] []
	//	[] []
	//	[] []
	int col, row;
	cout << "Entrer le nombre de colonne";
	cin >> col;
	cout << "Entrer le nombre de ligne";
	cin >> row;
	for (int r = 0; r < row; r++) {
		for (int c = 0; c < col; c++) {
			cout << "[ ] ";
		}
		cout << endl;
	}
}
