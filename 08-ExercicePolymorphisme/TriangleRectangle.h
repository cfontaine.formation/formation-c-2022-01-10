#pragma once
#include "Rectangle.h"
class TriangleRectangle : public Rectangle
{

public:
	TriangleRectangle(Couleur couleur, double largeur, double longueur) : Rectangle(couleur, largeur, longueur) {}


	double calculSurface() override;
};


