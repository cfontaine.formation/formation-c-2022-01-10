#include "Cercle.h"
#define _USE_MATH_DEFINES
#include <cmath>

double Cercle::calculSurface()
{
    return M_PI * std::pow(rayon, 2.0);
}
