#pragma once
#include "Forme.h"

class Cercle : public Forme
{
    double rayon;

public:
    Cercle(Couleur couleur, double rayon) : Forme(couleur), rayon(rayon) { }

    double calculSurface() override;
};

