#pragma once
#include "Forme.h"

class Terrain
{
    Forme* forme[10];
    int size;

public:
    Terrain() : size(0)
    {
        for (int i = 0; i < 10; i++)
        {
            forme[i] = nullptr;
        }
    }

    void ajoutForme(Forme& forme);

    double surfaceTotal();

    double surfaceTotal(Couleur couleur);
};
