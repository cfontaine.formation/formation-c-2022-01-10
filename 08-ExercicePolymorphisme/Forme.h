#pragma once
enum class Couleur { VERT, ORANGE, ROUGE, BLEU };

class Forme
{
	Couleur couleur;

public:

	Forme(Couleur couleur) :couleur(couleur) { }

	virtual double calculSurface() = 0;

	Couleur getCouleur() const
	{
		return couleur;
	}

};


