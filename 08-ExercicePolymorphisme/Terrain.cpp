#include "Terrain.h"

void Terrain::ajoutForme(Forme& forme)
{
	if (size < 10) {
		this->forme[size] = &forme;
		size++;
	}
}

double Terrain::surfaceTotal()
{
	double surfTotal = 0.0;
	for (int i = 0; i < size; i++)
	{
		surfTotal += forme[i]->calculSurface();
	}
	return surfTotal;
}

double Terrain::surfaceTotal(Couleur couleur)
{
	double surfTotal = 0.0;
	for (int i = 0; i < size; i++)
	{
		if (forme[i]->getCouleur() == couleur)
		{
			surfTotal += forme[i]->calculSurface();
		}
	}
	return surfTotal;
}
