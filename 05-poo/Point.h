#pragma once
class Point
{
	int x;
	int y;

public:
	Point() : x(0), y(0) {};

	Point(const int& x, const int& y) : x(x), y(y) {};

	int getX() const
	{
		return x;
	}

	int getY() const
	{
		return y;
	}

	void setX(const int& x)
	{
		this->x = x;
	}

	void setY(const int& y)
	{
		this->y = y;
	}

	void afficher() const;

	void deplacer(const int& tx, const int& ty);

	double norme() const;

	double distance(const Point& p) const;

	static double distance(const Point& p1, const Point& p2);
};
