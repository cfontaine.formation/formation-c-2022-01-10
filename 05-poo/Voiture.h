#pragma once
#include <string>
#include "Personne.h"
#include "Moteur.h"

class Voiture
{
	// Par defaut private
	// Variable d'instance
	std::string marque;
	std::string couleur;
	std::string plaqueIma;

protected:
	int vitesse = 0; // C++11

private:
	int compteurKm = 10; // C++11

	// agr�gation
	Personne* proprietaire;

	// composition
	Moteur moteur;

	// Variable de classe
	static int cptVoiture;

public:
	// Constructeurs

	// Constructeur d�faut
	Voiture() : Voiture("Opel", "Gris", "fg-1234-FR") {	};// constructeur d�l�gu� (en C++11)
    //Voiture() = default; // en C++11 cr�er un constructeur par d�faut

    // On peut surcharger les constructeurs
	Voiture(std::string marque, std::string couleur, std::string plaqueIma) : marque(marque), couleur(couleur), plaqueIma(plaqueIma), vitesse(0), compteurKm(10), proprietaire(nullptr), moteur(100)
	{
		cptVoiture++;
	};

	Voiture(std::string marque, std::string couleur, std::string plaqueIma, int compteurKm) : marque(marque), couleur(couleur), plaqueIma(plaqueIma), compteurKm(compteurKm), proprietaire(nullptr), moteur(100)
	{
		cptVoiture++;
	};


	// 1
	Voiture(std::string marque, std::string couleur, std::string plaqueIma, Personne* proprietaire) : marque(marque), couleur(couleur), plaqueIma(plaqueIma), proprietaire(proprietaire), moteur(100)
	{
		cptVoiture++;
	};

	// 2
	//Voiture(std::string marque, std::string couleur, std::string plaqueIma, Personne& proprietaire) : marque(marque), couleur(couleur), plaqueIma(plaqueIma), proprietaire(&proprietaire), moteur(100)
	//{
	//	cptVoiture++;
	//};

	// Constructeur par recopie
	// Il n'est pas n�cessaire de faire un constructeur par copie, si l'on n'a pas d'attribut allou� dynamiquement (pointeur)
	// si on veut supprimer la possibilit� de faire une copie de l'objet, on peut:
	// - uniquement le  d�clarer et ne pas donner de d�finition
	// - le rendre priv�
	Voiture(const Voiture& v); // ou Voiture(Voiture& v);

	// Destructeur
	~Voiture();

	/*private:
		Voiture(const Voiture& v);*/  // en C++ 98
		//	Voiture(const Voiture& v) = delete; // C++11
		// public:

	   // Le code plac� dans le .h est implicitement inline
	std::string getMarque() const
	{
		return marque;
	}

	// Getter et Setter
	std::string getCouleur() const	// Un m�thode peut �tre constante si elle ne modifie pas l'�tat de l'objet
	{								// Les m�thodes constantes sont les seules que l'on peut utiliser si l'objet est constant
		return couleur;
	}

	void setCouleur(const std::string& couleur)
	{
		this->couleur = couleur; // this permet de lever l'ambiguit� entre une variable d'instance et le param�tre
	}

	int getVitesse() const
	{
		return vitesse;
	}

	int getCompteurKm() const
	{
		return compteurKm;
	}

	// Une m�thode de classe ne peut pas �tre const
	static int getCptVoiture()
	{
		return cptVoiture;
	}

	// A
	Personne* getProprietaire()
	{
		return proprietaire;
	}

	// B
	//const Personne* getProprietaire()
	//{
	//	return proprietaire;
	//}

	// C
	//Personne& getProprietaire()
	//{
	//	return *proprietaire;
	//}

	void setProprietaire(Personne* proprietaire)
	{
		this->proprietaire = proprietaire;
	}

	// M�thodes d'instances
	void accelerer(int vAcc);
	void freiner(int vFrn);
	void arreter();
	bool estArreter() const;
	void afficher()  const;

	// M�thode de classe
	static void testMethodeClasse();
	static bool egaliterVitese(const Voiture& v1, const Voiture& v2);
};

