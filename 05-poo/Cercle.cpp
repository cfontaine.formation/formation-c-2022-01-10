#include "Cercle.h"

bool Cercle::colision(const Cercle& c1, const Cercle& c2)
{
    return Point::distance(c1.centre, c2.centre) <= (c1.rayon + c2.rayon);
}

bool Cercle::contenir(const Point &p)
{
    return Point::distance(centre, p) <= rayon;
}
