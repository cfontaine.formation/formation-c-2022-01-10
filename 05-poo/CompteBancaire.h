#pragma once
#include <string>

class CompteBancaire
{
protected:
	double solde;
private:
	std::string iban;
	std::string titulaire;

	static int compteurCompte;

public:

	CompteBancaire(const std::string &titulaire) : CompteBancaire(titulaire,0.0) {};

	CompteBancaire(const std::string &titulaire,double solde) : solde(solde), titulaire(titulaire){ /*, iban("fr-5962-0000-" + std::to_string(++compteurCompte))*/ 
		compteurCompte++;
		iban = "fr-5962-0000-" + std::to_string(compteurCompte);
	};

	CompteBancaire(const CompteBancaire&) = delete;

	double getSolde() const
	{
		return solde;
	}

	std::string getIban() const
	{
		return iban;
	}

	std::string getTitulaire() const
	{
		return titulaire;
	}

	void setTitulaire(const std::string &titulaire)
	{
		this->titulaire = titulaire;
	}

	void afficher() const;
	
	void crediter(double valeur);
	
	void debiter(double valeur);

	bool estPositif() const
	{
		return solde > 0.0;
	}
};


