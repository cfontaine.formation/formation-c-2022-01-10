#include "VoiturePrioritaire.h"
#include <iostream>
using namespace std;

// Red�finition de la m�thode afficher
void VoiturePrioritaire::afficher()
{
	Voiture::afficher();	// appel de la m�thode afficher de la classe m�re 
	cout << "gyro="<<gyro << endl;
}

// Red�finition de la m�thode accelerer
void VoiturePrioritaire::accelerer(int vAcc2)
{
	vitesse += vAcc2; // on peut acc�der � la variable d'instance vitesse de la classe m�re car elle a pour visibilit� protected
}
