#include "CompteEpargne.h"
#include <iostream>

using namespace std;

void CompteEpargne::calculInterets()
{
	solde *= (1.0 + taux / 100.0);
}

void CompteEpargne::afficher() const
{
	CompteBancaire::afficher();

	cout << "taux=" << taux<<endl;
	cout << "________________" << endl;
}
