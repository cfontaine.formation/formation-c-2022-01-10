#include <iostream>
#include <cmath>
#include "Point.h"

using namespace std;

void Point::afficher() const
{
    cout << "(" << x << "," << y << ")" << endl;
}

void Point::deplacer(const int &tx, const int &ty)
{
    x += tx;
    y += ty;
}

double Point::norme() const
{
    return sqrt(pow(x,2) + pow(y,2));
}

double Point::distance(const Point& p) const
{

    return sqrt(pow((p.x - x), 2) + pow((p.y - y), 2));
}

double Point::distance(const Point& p1, const Point& p2)
{
    return sqrt(pow((p2.x -p1.x), 2) + pow((p2.y - p1.y), 2));
}
