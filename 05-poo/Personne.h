#pragma once
#include<string>

class Personne
{
	std::string prenom;

	std::string nom;

public:
	Personne(std::string prenom, std::string nom) :prenom(prenom), nom(nom) {};

	std::string getNom() const
	{
		return prenom;
	}

	std::string getPrenom() const
	{
		return prenom;
	}

	void setPrenom(const std::string prenom)
	{
		this->prenom = prenom;
	}

	void afficher() const;
};

