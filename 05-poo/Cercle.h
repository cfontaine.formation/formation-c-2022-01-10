#pragma once
#include "Point.h"

class Cercle
{
	Point &centre;
	double rayon;
public:
	Cercle(Point& centre, double rayon) : centre(centre), rayon(rayon)
	{

	}

	Point& getCentre() const
	{
		return centre;
	}

	double getRayon() const
	{
		return rayon;
	}

	static bool colision(const Cercle& c1, const Cercle& c2);

	bool contenir(const Point &p);

};

