#include "CompteBancaire.h"
#include <iostream>

using namespace std;

int CompteBancaire::compteurCompte = 0;

void CompteBancaire::afficher() const
{
	cout << "________________" << endl;
	cout << "Solde = " << solde << endl;
	cout << "Iban = " << iban << endl;
	cout << "Titulaire" << titulaire << endl;
	cout << "________________" << endl;
}

void CompteBancaire::crediter(double valeur)
{
	if (valeur > 0)
	{
		solde += valeur;
	}
}

void CompteBancaire::debiter(double valeur)
{
	if (valeur > 0)
	{
		solde -= valeur;
	}
}
