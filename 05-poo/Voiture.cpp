#include<iostream>
#include "Voiture.h"
using namespace std;

// Initialisation de la variable de classe
int Voiture::cptVoiture = 0;

//Voiture::Voiture(std::string marque, std::string couleur, std::string plaqueIma)
//{
//	this->marque = marque;
//	this->couleur = couleur;
//	this->plaqueIma = plaqueIma;
//	this->vitesse = 0;
//	this->compteurKm = 10;
//}*

// Constructeur par copie
Voiture::Voiture(const Voiture& v) 
{
	cout << "Constructeur par copie" << endl;
	marque = v.marque;
	couleur = v.couleur;
	plaqueIma = v.plaqueIma;
	vitesse = v.vitesse;
	compteurKm = v.compteurKm;
	moteur = v.moteur;
	cptVoiture++;
}

Voiture::~Voiture()
{
	cout << "Destructeur" << endl;
	cptVoiture--;
}

void Voiture::accelerer(int vAcc)
{
	if (vAcc > 0)
	{
		this->vitesse += vAcc;
	}
}

void Voiture::testMethodeClasse()
{
	cout << "M�thode de classe" << endl;
	// cout << vitesse << endl;	// dans une m�thode de classe,e on ne peut pas acc�der au variables d'instances
	// freiner();				// ni aux m�thodes d'instances
	cout << cptVoiture << endl;
}

bool Voiture::egaliterVitese(const Voiture& v1, const Voiture& v2)
{
	// Dans un m�thode de classe, on peut uniquement acc�der � une variable d'instance
	// par l'interm�diaire d' objet pass� en param�tre
	return v1.vitesse == v2.vitesse;
}


void Voiture::freiner(int vFrn)
{
	if (vFrn > 0)
	{
		vitesse -= vFrn;
	}
}

void Voiture::arreter()
{
	vitesse = 0;
}

bool Voiture::estArreter() const
{
	return vitesse == 0;
}

void Voiture::afficher() const
{
	cout << "[" << marque << " " << couleur << " " << plaqueIma << " " << vitesse << " " << compteurKm << " ]"<<endl;
	if (proprietaire)
	{
		proprietaire->afficher();
	}
}
