#pragma once
#include "CompteBancaire.h"
class CompteEpargne : public CompteBancaire
{
	double taux;

public:
	CompteEpargne(std::string titulaire, double solde, double taux) : CompteBancaire(titulaire, solde), taux(taux) {}

	double getTaux() const {
		return taux;
	}

	void setTaux(double taux) {
		this->taux = taux;
	}

	void calculInterets();

	void afficher() const;


};

