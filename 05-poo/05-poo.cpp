
#include <iostream>
#include "voiture.h"
#include "CompteBancaire.h"
#include "Point.h"
#include "Cercle.h"
#include "VoiturePrioritaire.h"
#include "CompteEpargne.h"

using namespace std;

int main()
{
	// Acc�s � une variable de classe
	cout << "Nombre de voiture=" << Voiture::getCptVoiture() << endl; // Voiture::cptVoiture

	Voiture::testMethodeClasse();

	// Instanciation statique
	Voiture v1;
	//  v1.vitesse = 10;
	cout << v1.getVitesse() << endl; // v1.vitesse 
	cout << "Nombre de voiture=" << Voiture::getCptVoiture() << endl;

	// Appel d'une m�thode d'instance
	v1.accelerer(20);
	v1.afficher();
	v1.freiner(15);
	v1.afficher();
	cout << v1.estArreter() << endl;
	v1.arreter();
	cout << v1.estArreter() << endl;
	v1.afficher();

	// Instanciation dynamique
	Voiture* v2 = new Voiture;
	// v2->vitesse = 0;
	cout << v2->getVitesse() << endl; // v2->vitesse 
	cout << "Nombre de voiture=" << Voiture::getCptVoiture() << endl;
	cout << Voiture::egaliterVitese(v1, *v2) << endl;
	delete v2;
	cout << "Nombre de voiture=" << Voiture::getCptVoiture() << endl;

	Voiture v3("Fiat", "Jaune", "AZ-5678-FR");
	v3.accelerer(20);
	cout << Voiture::egaliterVitese(v1, v3) << endl;
	v3.afficher();
	cout << "Nombre de voiture=" << Voiture::getCptVoiture() << endl;

	// Appel du constructeur par copie
	Voiture v4(v3);
	//Voiture v4{ v3 }; // en C++11
	//Voiture v4 = { v3 };
	v4.afficher();
	cout << "Nombre de voiture=" << Voiture::getCptVoiture() << endl;

	// Agr�gation
	Personne per1("John", "Doe");
	v3.setProprietaire(&per1);
	v3.afficher();

	// A
	v3.getProprietaire()->setPrenom("Jack");

	// B
	// const Personne* pc = v3.getProprietaire();
	// v3->setPrenom("Jack"); // Erreur on ne peut pas modifier l'objet
	// pc->afficher();

	// C
	//Personne pc2 = v3.getProprietaire();
	//pc2.afficher();

	// 1
	Voiture v5("Toyota", "blanc", "rhqgif", &per1);
	v5.afficher();

	// 2
	//Voiture v5("Toyota", "blanc", "rhqgif", per1);
	//v5.afficher();


	// H�ritage
	VoiturePrioritaire vp("Fiat", "jaune", "qd-6794-fr", true);
	vp.afficher();
	cout << vp.isGyro() << endl;
	vp.accelerer(30);
	vp.afficher();

	// Premier essai polymorphisme (sans virtual)
	Voiture* vpo = &vp;
	vpo->afficher(); // la m�thode afficher de voiture qui est appel�

	// CompteBancaire
	CompteBancaire cb1("John Doe");
	cb1.afficher();

	CompteBancaire* cb2 = new CompteBancaire("Alan Smithee", 250.0);
	cb2->afficher();
	delete cb2;

	// Exercice h�ritage : Compte �pargne
	CompteEpargne ce("John Doe", 100.0, 5.0);
	ce.afficher();
	ce.crediter(500.0);
	ce.afficher();
	ce.calculInterets();
	ce.afficher();

	//  Exercice: Point 
	Point p1(1, 2);
	p1.afficher();
	Point p2(1, 1);
	p2.afficher();
	p2.deplacer(1, 1);
	p2.afficher();
	cout << "Norme=" << p1.norme() << endl;
	cout << "Distance=" << Point::distance(p1, p2) << endl;

	// Exercice agr�gation: Cercle 
	Point ce1(2, 0);
	Point ce2(6, 0);
	Cercle c1(ce1, 2.0);
	Cercle c2(ce2, 1.0);
	Cercle c3(ce2, 3.0);
	cout << Cercle::colision(c1, c2) << endl; // false
	cout << Cercle::colision(c1, c3) << endl; // true
	Point po(10, 10);
	Point pi(1, 1);
	cout << c1.contenir(po) << endl;
	cout << c1.contenir(pi) << endl;
}
