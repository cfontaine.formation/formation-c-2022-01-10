#pragma once
template<typename T>class Pair
{
	T a;
	T b;

public :
	Pair(T a, T b) : a(a), b(b) {}

	T maximum()
	{
		return a > b ? a : b;
	}

	T minimum()
	{
		return a > b ? b : a;
	}

};