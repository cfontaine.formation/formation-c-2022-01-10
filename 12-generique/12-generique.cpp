#include <iostream>
#include <string>
#include "Pair.h"

using namespace std;

template<typename T> T somme(T a, T b)
{
    return a + b;
}

int main()
{
    // Fonction générique
    cout << somme<int>(1, 2) << endl;
    cout << somme(1L, 2L) << endl;
    cout << somme(1.5, 2.8) << endl;
    cout << somme(string("aze"), string("rty")) << endl;

    // Classe générique
    Pair<int> pi(1, 2);
    cout << pi.minimum() << endl;
    Pair<string> ps("aze", "rty");
    cout << ps.minimum() << endl;

}
