#include <iostream>
#include "AgeNegatifException.h"
using namespace std;

void traitementAge(int age);
void etatCivil(int age);
void test() noexcept;

int main()
{
    int age;
    cin >> age;
    try
    {
        etatCivil(age);
        cout<<"reste du programme"<<endl;
    }
    catch (AgeNegatifException& err) {
        cerr << err.what() <<"Main"<<endl;
    }
    catch (int)
    {
        cerr << "exception int" << endl;
    }
    catch (...)
    {
        cerr << "autre exception" << endl;
    }
    cout << "fin du programme" << endl;
}

void traitementAge(int age) // c++98 Tout
{
    cout << "Début du traitement" << endl;
    if (age < 0)
    {
        throw AgeNegatifException(age);
    }
    else if (age == 0)
    {
        throw 'n';
    } 
    cout << "fin traitement" << endl;
}

void etatCivil(int age) throw(int,char) // C++98  
{
    cout << "début etat civil" << endl;
    try {
        traitementAge(age);
    }
    catch (AgeNegatifException& e) 
    {
        cout << "traitement partiel age: etat civil" << endl;
        cerr << e.what() << endl;
       // throw;
        throw 1;
    }
    cout << "début etat civil" << endl;
}

void test() noexcept // c++11 noexcept ou C++98 throw() pas d'exception
{
    cout << "test" << endl;
}