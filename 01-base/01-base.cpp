#include <iostream>
#include <bitset>
using namespace std;

// Variable globale
int vGlobal = 123;

int main()
{
	// Déclaration variable
	int i;

	// Initialisation variable
	i = 42;
	cout << i << endl;

	// Déclaration et initialisation multiple
	int mb, mc = 12;
	mb = 12;
	cout << mb << " " << mc << endl;

	// Déclarer et initialiser une variable
	int j = 10;
	int jCpp98(10);		// C++
	int jCpp11{ 10 };	// C++11
	int jCpp11B = { 10 };
	cout << j << "  " << jCpp98 << "  " << jCpp11 << "  " << jCpp11B << endl;

	// Littéral entier -> par défaut int
	long l = 10L;			// litéral long -> L
	unsigned int u = 10U;	// litéral unsigned -> U
	long long lli = 100LL;	// litéral long long -> LL en C++ 11
	cout << l << " " << u << " " << lli << endl;

	// Changement de base
	int decimal = 42;      // base 10 par défaut: décimal
	int hexa = 0xFF12A;    // base 16 héxadécimal -> 0x
	int octal = 0341;      // base 8  octal -> 0
	int bin = 0b101011010; // base 2  binaire -> 0b en C++14
	cout << decimal << " " << hex << hexa << " " << oct << octal << bitset<32>(bin) << dec << endl;

	// Littéral virgule flottante
	double d1 = 1.5;
	double d2 = 5.;
	double d3 = .55;
	double d4 = 1.334e03;
	cout << d1 << " " << d2 << " " << d3 << " " << d4 << endl;

	// Littéral virgule flottante -> par défaut de type double
	float f = 123.4F;		// littéral float -> F
	long double ld = 1.23L;	// littéral long double -> L

	// Littéral booléen
	bool test = false; // ou true

	 // Littéral caractère 
	char c = 'a';
	char cHexa = '\x41';	// caractère en hexadécimal
	char cOctal = '\32';	// caractère en octal

	// Variable globale
	cout << vGlobal << endl; // affiche 123
	// On déclare une variable locale qui a le même le nom que la variable globale
	int vGlobal = 12;
	// La variable globale est masquée par la variable locale
	cout << vGlobal << endl;
	// Pour accéder à la variable globale, on utilise l’opérateur de résolution de portée ::
	cout << ::vGlobal << endl; // affiche 123

	// Constante
	const double PI = 3.14;
	//PI = 3.149;	// Erreur: on ne peut pas modifier une constante

	// constExpr
	constexpr double cexp = PI * 2.0;
	cout << cexp << endl;

	const double p = 3.14;
	const double PI2 = p;
	/*constexpr*/ double cexp2 = PI2 * 2;
	cout << cexp2 << endl;

	// Typage implicite C++ 11
	// Le type de la variable est définie à partir de l'expression d'initialisation
	auto implicite = 'a';		// implicite de type char
	auto implicite2 = 12.3f;	// implicite2 de type float
	// Attention aux chaine de caratères: avec auto le type sera const char* et pas std::string
	auto impliciteStr = "azerty"; // const char* 
	int tab[2] = { 1,2 };
	auto impl = tab;
	auto implicite3 = PI; // implicite3 de type double const et constexpr ne sont pas pris en compte avec auto

	// decltype  C++ 11 -> déclarer qu'une variable est de même type qu'une autre
	decltype (c) c2 = 'z';		// c2 de type char
	decltype (PI) p3 = 3.14;	// p3 de type double
	decltype(3 * PI + 12.5) p4 = 12.4;	// p4 de type double

	// Opérateur
	// Opérateur arithmétique
	int aa = 1;
	int bb = 3;
	int res = aa + bb;
	cout << res << "  " << bb % 2 << endl; // % => modulo (reste de division entière) uniquement avec des entiers positif

	// Pré-incrémentation
	int inc = 0;
	res = ++inc; // inc=1 res=1
	cout << inc << " " << res << endl;

	// Post-incrémentation
	inc = 0;
	res = inc++; // res=0 inc =1
	cout << inc << " " << res << endl;

	// Affection composée
	res = 12;
	res += 10; // res=res+10

	// Opérateur de comparaison
	int val;
	cin >> val;
	bool tst1 = val > 10;	 // Une comparaison a pour résultat un booléen
	cout << tst1 << endl;

	// Opérateur logique
	// non
	bool inv = !tst1;
	cout << inv << endl;

	// Opérateur court-circuit && et || (évaluation garantie de gauche à droite)
	// && dés qu'une comparaison est fausse, les suivantes ne sont pas évaluées
	// || dés qu'une comparaison est vrai, les suivantes ne sont pas évaluées
	bool tst2 = val < 50 && val++>20;	// si val < 50 est faux,  val++>20 n'est pas évalué
	cout << val << endl;
	tst2 = val > 50 || val++ < 20;		// si val > 50 est vrai,  val++>20 n'est pas évalué
	cout << val << endl;

	// Opérateur binaire (uniquement avec des entiers)
   // Pour les opérations sur des binaires, on utilise les entiers non signé
	unsigned int b1 = 0b101011;
	cout << ~b1 << " " << bitset<32>(~b1) << endl;					// Complémént: 1 -> 0 et 0 -> 1
	cout << (b1 & 0b10) << " " << bitset<32>(b1 & 0b10) << endl;	// Et bit à bit	 2 10
	cout << (b1 | 0b100) << " " << bitset<32>(b1 | 0b100) << endl;  // Ou bit à bit 47 101111
	cout << (b1 ^ 0b110) << " " << bitset<32>(b1 ^ 0b110) << endl;	// Ou exclusif bit à bit 45 101101

	cout << (b1 >> 1) << endl; // Décalage à droite de 1 10101
	cout << (b1 << 1) << endl; // Décalage à gauche de 1 1010110
	cout << (b1 >> 3) << endl; // Décalage à droite de 3 101

	// Opérateur sizeof
	cout << sizeof(b1) << endl;		// nombre d'octets de la variable b1 -> 4
	cout << sizeof(bool) << endl;	// nombre d'octets du type bool -> 1

	// Opérateur séquentiel, toujours évalué de gauche->droite
	inc = 0;
	int o = 12;
	res = (inc++, o + 10);
	cout << inc << " " << res << endl;

	// Conversion implicite -> rang inférieur vers un rang supérieur (pas de perte de donnée)
	int convI = 12;
	double convD = 23.5;
	double cres = convI + convD;

	// Promotion numérique -> short, bool ou char dans une expression -> convertie automatiquement en int
	short s1 = 12;
	short s2 = 4;
	int s3 = s1 + s2;
	cout << s3 << endl;

	// boolean convertion en entier
	int convI1 = false; // 0
	int convI2 = true;	// S1

	// entier convertion en boolean
	bool convB1 = 34; // true
	bool convB2 = 0;  // false
	cout << convB1 << " " << convB2 << endl;

	// Convertion explicite
	int te = 11;
	int div = 2;
	cout << (te / div) << endl;							// 5
	cout << (((double)te) / div) << endl;				// 5.5 
	cout << (static_cast<double>(te) / div) << endl;	// 5.5 // en C++ -> le compilateur fait plus de vérification (sur le type)

	// Opérateur d'affectation conversion systémathique (implicite et explicite
	double affD = 34.5;
	int affI = affD; // 34

	// Dépassement de capacité
	int dep = 300;
	char cDep = dep;	           // 0001 0010 1100	300 => 300 > 127 ou 255, la valeur maximal d'un char
	cout << static_cast<int>(cDep) << endl;

	// division par 0
	int div1 = 10;
	int div2 = 0;
	//cout << (div1 / div2) << endl;

	double divD1 = 10.0;
	double divD2 = 0.0;
	cout << (divD1 / divD2) << endl; //+INF,-INF,NAN
}
