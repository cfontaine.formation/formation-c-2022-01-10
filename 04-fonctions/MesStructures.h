#pragma once
#include <string>

struct Contact
{
	// Champs
	std::string prenom;
	std::string nom;
	std::string telephone;
	mutable int age;	// mutable => si la structure est constante,
						// on pourra quand m�me modifier les champs mutable

	// Fonction
	void afficher();	// En c++ une structure peut contenir des fonctions
};