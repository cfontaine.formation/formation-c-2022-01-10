// Fichier .h -> contient la d�claration des fonctions

#ifndef MES_FONCTIONS_H // Indique au compilateur de n'int�grer le fichier d�en-t�te qu�une seule fois, lors de la compilation d�un fichier de code source
#define MES_FONCTIONS_H // ou #pragma once
#include <string>

// d�claration d'une variable qui a �t� d�finie dans main.cpp. Il n' y a pas d'allocation m�moire
extern const int testVarExten;

//extern double pi // pi est static dans 04-fonctions.cpp, on ne peut l'utiliser que dans ce fichier;

const int testExt = 123;

// D�claration d'une fonction
int somme(int, int);

// D�claration d'une fonction sans retour => void
void afficher(int);

// Passage de param�tre par valeur
void testParamValeur(int);

// On peut d�finir des valeurs par d�faut pour les param�tres. Ils doivent se trouver en fin de liste des arguments
// On place les valeurs par d�faut dans la d�claration des fonctions
void testParamDefault(int i, double d = 3.0, char c = 'a');

// Passage de param�tre par adresse
// En utilisant le passage de param�tres par adresse, on modifie r�ellement la variable qui est pass�e en param�tre
void testParamAdresse(int* a);

// Passage d'un tableau, comme param�tre d'une fonction
// On ne peut pas passer un tableau par valeur uniquement par adresse
void afficherTab(int tab[], int size);
// ou void afficherTab(int* tab, int size);

// Pointeur de pointeur
void creerTab(int** t, int size, int val);

// Passage de param�tre par r�f�rence
// Comme avec  le passage de param�tres par adresse, on modifie r�ellement la variable qui est pass�e en param�tre
void testParamRef(int& a);
void testParamRefCst(const int& cst = 2);

// Exercice maximum
double maximum(double a, double b);

// Pour les fonctions inline, on place le code de la fonction dans le fichier.h
inline int doubler(int a)
{
	return 2 * a;
}

// Exercice parit�
// Les fonctions constexpr sont implicitement inline
constexpr bool even(int valeur)
{
	return (valeur & 1) == 0; //valeur % 2 == 0;
}

// Surcharge de m�thode
int multiplier(int v1, int v2);
double multiplier(double v1, double v2);
double multiplier(int v1, double v2);
int multiplier(int v1, int v2, int v3);
int multiplier(int* a, int b);
double multiplier(double& a, int b);
double multiplier(const double& a, int b);

// R�cursivit�
int factorial(int n);

// Pointeur de fonction
// typedef permet de d�finir des synonymes de types -> typedef type synonyme_type;
typedef double (*ptrFonction)(double, double);
double addition(double a, double b);
double multiplication(double a, double b);
void affCalcul(double v1, double v2, ptrFonction pf);

// Test de classe de m�morisation
void TestMemStatic();
void TestMemExtern();

// ERREUR => fonctions qui retournent une r�f�rence ou un pointeur sur une variable locale
std::string* testRetour(int n);

// Exercices chaine de caract�re
void unSurDeux(const std::string& str);
std::string inverser(const std::string& str);
bool palindrome(const std::string& str);

// Fonction � nombre variable de param�tre  (h�rit� du C)
double moyenne(int nbArgs, ...);
#endif