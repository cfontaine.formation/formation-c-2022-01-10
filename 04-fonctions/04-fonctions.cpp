#include <iostream>
#include "MesFonctions.h"
#include "MesStructures.h"
using namespace std;

// D�claration des fonctions -> D�placer dans MesFonctions.h
// int somme(int ,int);
// void afficher(int a);

const int testVarExten = 123;

static double pi = 3.14;    // avec static la variable n'est visible que dans ce fichier

int main(int argc, char* argv[])
{
	// Appel de methode
	cout << somme(1, 2) << endl;

	// Appel de methode (sans retour)
	afficher(4);
	afficher(5);

	// Param�tre pass� par valeur
	// C'est une copie de la valeur du param�tre qui est transmise � la m�thode
	int v = 1;
	testParamValeur(v);
	cout << v << endl;

	// Param�tre par d�faut (uniquement pour les param�tres pass�s par valeur)
	testParamDefault(1);
	testParamDefault(2, 3.14);
	testParamDefault(2, 4.5, 'c');

	// Param�tre pass� par adresse
	// La valeur de la variable pass�e en param�tre est modifi�e, si elle est modifi�e dans la m�thode
	testParamAdresse(&v);   // on passe en param�tre l'adresse de la variable p
	cout << v << endl;

	// Les tableaux peuvent �tre passer par adresse et pas par valeur
	int t[] = { 3,5,2,8,4 };
	afficherTab(t, 5);  // le nom du tableau correspond � un pointeur sur le premier �l�ment du tableau

	// Pointeur de pointeur
	int* tp;
	creerTab(&tp, 3, 5);
	afficherTab(tp, 3);

	// Param�tre pass� par r�f�rence
	testParamRef(v);
	cout << v << endl;

	// avec une r�f�rence constante, on peut aussi passer des litt�rals
	testParamRefCst(v);
	testParamRefCst(45);
	testParamRefCst();

	//// Exercice Fonction maximum
	//double d1, d2;
	//cin >> d1 >> d2;
	//cout << "Maximum=" << maximum(d1,d2) << endl;

	// Exercice Fonction parit�
	// Fonction constexpr
	// L'appel d'une fonction constexpr peut-�tre remplac� par la valeur �valu�e lors de la compilation
	cout << even(3) << endl; // remplacer par false
	cout << even(4) << endl; // remplacer par true
	cout << even(v) << endl; // sinon elle consid�r� comme fonction normal 

	// Fonction inline
	// Chaque appel une fonction inline va �tre remplac� par le corps de la fonction
	cout << doubler(5) << endl;

	// Surcharge de fonction
	cout << multiplier(1, 1) << endl;
	cout << multiplier(3.5, 1.3) << endl;
	cout << multiplier(1, 1.6) << endl;
	cout << multiplier(1, 2, 3) << endl;
	int param = 19;
	cout << multiplier(&param, 1) << endl;

	cout << multiplier('\02', '\04') << endl;   // Convertion des caract�res en entier
	cout << multiplier(1, 1.8f) << endl;        // Convertion du float en double


	// Les arguments double& et const double& sont consid�r�s comme diff�rents	
	cout << multiplier(1.6, 1) << endl;
	double param2 = 12.3;
	cout << multiplier(param2, 1) << endl;

	// Fonction r�curssive
	int req = factorial(3);
	cout << req << endl;

	// Param�tre de la fonction main
	for (int i = 0; i < argc; i++)
	{
		cout << argv[i] << endl;
	}

	// Pointeur de fonction
	double (*ptrf)(double, double);
	ptrf = addition;
	cout << (*ptrf)(1.2, 3.4) << endl;
	cout << ptrf(5.6, 7.8) << endl;

	affCalcul(2.0, 3.5, addition);
	affCalcul(2.0, 3.5, multiplication);

	// /!\ une fonction ne doit pas retourner une r�f�rence ou un pointeur sur une variable locale � une fonction
	string* retStr = testRetour(5);
	cout << retStr << endl;
	cout << *retStr << endl;
	delete retStr;

	// Classe de m�morisation
	TestMemStatic();
	TestMemStatic();
	TestMemStatic();
	TestMemStatic();

	TestMemExtern();

	cout << testExt << endl;

	// Structure
	// struct Contact c1; // en C
	Contact c1; // en C++ struct n'est pas n�c�ssaire
   // std::cout << c1.age << endl;
	c1.prenom = "John";
	c1.nom = "Doe";
	c1.telephone = "03.20.00.00.00";
	c1.age = 37;
	c1.afficher();

	Contact c2 = { "Jane", "Doe","03.20.00.00.00",29 };
	c2.afficher();

	Contact c3 = c2;
	c3.afficher();
	c3.age = 50;
	c2.afficher();
	c3.afficher();

	// On teste l'�galit� de 2 structures champs par champs
	bool test = c1.age == c2.age && c1.nom == c2.nom && c1.prenom == c2.prenom && c1.telephone == c2.telephone;

	Contact* ptrC = new Contact;
	(*ptrC).nom = "Smithee";
	ptrC->prenom = "alan";
	ptrC->age = 49;
	ptrC->telephone = "03.27.00.00.00";
	ptrC->afficher();
	delete ptrC;

	const Contact cCst = { "Joe","Dalton","03.20.00.00.01",37 };
	cout << cCst.age << endl;
	// cCst.prenom = "Jack";
	cCst.age = 40; // champs mutable -> on peut le modifier m�me si l'objet est constant

	// Chaine de caract�re C
	// en C, un chaine de caract�re est un tableau de caract�re qui est termin� par un caract�re terminateur '\0'
	const char* strCstC = "Helloworld"; // Chaine constante -> const char*
	//strCstC[2] = i;
	cout << strCstC << endl;

	char strC[] = "Bonjour";
	strC[0] = 'b';
	cout << sizeof(strC) << endl; // Nombre de caract�res de la chaine +1 pour le terminateur \0
	cout << strC << endl;

	// en C++ string
	string str = "Hello";
	cout << str << endl;
	string str1 = string("bonjour");
	string str2 = string(10, 'a');
	cout << str1 << " " << str2;

	string* str4 = new string("azerty");
	cout << *str4 << endl;
	delete str4;

	// Concat�nation
	str += " World";     // Op�rateur + et += concat�nation de chaine
	cout << str << endl;
	str.append(" !!!!"); // append -> permet d'ajouter la chaine pass�e en param�tre en fin de chaine
	cout << str << endl;
	str.push_back('?');  // push_back -> ajoute le caract�re pass� en param�tre en fin de chaine
	cout << str << endl;

	// Comparaison
	// On peut utiliser les op�rateurs de comparaison avec les chaines de caract�res
	if (str1 == str2)
	{
		cout << "==" << endl;
	}
	else
	{
		cout << "!=" << endl;
	}

	if (str2 < str1)
	{
		cout << "inf�rieur" << endl;
	}

	cout << str.length() << endl;   // length -> nombre de caract�res de la chaine (idem pour size)
	cout << str.empty() << endl;    // empty -> retourne true si la chaine est vide

	// Acc�s � un cararact�re de la chaine [] ou at
	cout << str[1] << endl;
	cout << str.at(1) << endl;
	// cout << str[40] << endl;
	// cout << str.at(40) << endl;   // avec at si l'indice d�passe la taille de la chaine une exception est lanc�e

	// Extraire une sous-chaine
	cout << str.substr(5) << endl;      // substr -> retourne une  sous-chaine qui commence � l'indice 5 et jusqu'� la fin
	cout << str.substr(6, 5) << endl;    // substr -> retourne une  sous-chaine de 5 caract�res qui commence � l'indice 6

	str.insert(5, "-----");         // ins�re la chaine pass�e en param�tre � la position 5
	cout << str << endl;

	//str.replace(6, 1, "_?!");   // replace -> remplace � la position 6 , 1 caract�re par la chaine de caract�re pass� en param�tre
	cout << str << endl;

	str.erase(5, 5);    // supprimer 5 caract�res de la chaine � partir de la position 5
	cout << str << endl;
	str.erase(12);      // supprimer les caract�res de la chaine � partir de la position 12 jusqu'� la fin de la chaine
	cout << str << endl;

	// Recherche de la position de la chaine ou du caract�re pass�e en param�tre dans la chaine str
	cout << str.find('o') << endl;                      // � partir du d�but de la chaine
	cout << str.find('o', 5) << endl;                    // � partir de la position 5
	cout << str.find('o', 8) << " " << string::npos << endl; // � partir de la position 8, pas de caract�re => retourne la valeur std::string::npos

	cout << str.rfind('o') << endl; // rfind => idem find, mais on commence � partir de la fin de la chaine et on va vers le debut

	cout << str.c_str() << endl;    // convertion string en chaine C (const *char)

	// Iterateur = > objet qui permet de parcourir la chaine(un peu comme un pointeur)
	// begin => retourne un iterateur sur le d�but de la chaine
	// end => retourne un iterateur sur la fin de la chaine
	for (string::iterator it = str.begin(); it != str.end(); it++)  // it++ permet de passer au caract�re suivant
	{
		cout << *it << endl;    // *it permet d'obtenir le caract�re "pointer" par l'it�rateur
	   // *it = 'a';    // on a acc�s en lecture et en �criture
	}

	// rbegin et rend idem  mais l'it�rateur part de la fin et va vers le d�but de la chaine
	for (auto it = str.rbegin(); it != str.rend(); it++)    // it++ permet de passer au caract�re pr�c�dent
	{
		cout << *it << endl;
	}

	// C++11
	for (auto chr : str) {
		cout << chr << endl;
	}

	// C++11
	// Diff�rence rbegin / crbegin
	// crbegin -> on a acc�s uniquement en lecture par l'interm�diaire de l'itt�rateur
	for (auto it = str.cbegin(); it != str.cend(); it++)
	{
		cout << *it << endl;
		// *it = 'a';
	}

	str.clear();                    // Efface les caract�res contenus dans la chaine
	cout << str.empty() << endl;    // empty => retourne true si la chaine est vide

	// Exercices
	unSurDeux("bonjour");
	cout << inverser("bonjour") << endl;
	cout << palindrome("bonjour") << endl;
	cout << palindrome("radar") << endl;

	// Fonction � nombre variable de param�tre  (h�rit� du C)
	cout << moyenne(0) << endl;
	cout << moyenne(1, 2) << endl;
	cout << moyenne(5, 2, 3, 5, 2, 6) << endl;


	return 0;
}


// D�finition des fonctions => D�placer dans MesFonctions.cpp
//int somme(int a, int b)
//{
//    return a + b;
//}
//
//void afficher(int a)
//{
//    cout << a << endl;
//    // return;
//}