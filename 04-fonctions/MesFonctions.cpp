#include <iostream>
#include<cstdarg>
#include "MesFonctions.h"
using namespace std;

// Fichier .cpp  -> contient les D�finitions des fonctions


int somme(int a, int b)
{
	return a + b;
}

void afficher(int a)
{
	cout << a << endl;
	// return;
}

void testParamValeur(int a)
{
	cout << a << endl;
	a = 123;
	cout << a << endl;
}

// Les param�tres par d�faut sont uniquement d�fini dans la d�claration de la fonction (.h)
void testParamDefault(int i, double d, char c)
{
	cout << i << " " << d << " " << c << endl;
}

// En utilisant le passage de param�tres par adresse, on modifie r�ellement la variable qui est pass�e en param�tre
void testParamAdresse(int* a)
{
	cout << *a << endl;
	*a += 20;
}

void afficherTab(int tab[], int size)
{
	cout << "[ ";
	for (int i = 0; i < size; i++)
	{
		cout << tab[i] << " ";
	}
	cout << "]" << endl;
}

void creerTab(int** t, int size, int val)
{
	*t = new int[size + 2];
	for (int i = 0; i < size; i++) {
		(*t)[i] = val;
	}
}

// Passage de param�tre par r�f�rence
// Comme avec le passage de param�tres par adresse, on modifie r�ellement la variable qui est pass�e en param�tre
void testParamRef(int& a)
{
	a = 12;
}

void testParamRefCst(const int& cst)
{
	cout << cst * 2 << endl;
}

double maximum(double a, double b)
{
	return a > b ? a : b;
}

// Surcharge de fonction
// Plusieurs fonctions peuvent avoir le m�me nom
// Ces arguments doivent �tre diff�rents en nombre ou/et en type pour qu'il n'y ai pas d'ambiguit� pour le compilateur
int multiplier(int v1, int v2)
{
	cout << "2 int" << endl;
	return v1 * v2;
}

double multiplier(double v1, double v2)
{
	cout << "2 double" << endl;
	return v1 * v2;
}

double multiplier(int v1, double v2)
{
	cout << "1 int 1 double" << endl;
	return v1 * v2;
}

int multiplier(int v1, int v2, int v3)
{
	cout << "3 int" << endl;
	return v1 * v2;
}

int multiplier(int* a, int b)
{
	cout << "2 int pointeur" << endl;
	return *a * b;
}

double multiplier(double& a, int b)
{
	return a * b;
}

double multiplier(const double& a, int b)
{
	return a * b;
}


// Fonction r�cursive
int factorial(int n) // factoriel= 1* 2* � n
{
	if (n <= 1) // condition de sortie
	{
		return 1;
	}
	else
	{
		return factorial(n - 1) * n;
	}
}


// Pointeur de fonction
double addition(double a, double b)
{
	return a + b;
}

double multiplication(double a, double b)
{
	return a * b;
}

void affCalcul(double v1, double v2, ptrFonction pf)
{
	cout << pf << endl;
	if (pf != nullptr) {
		cout << pf(v1, v2) << endl;
	}

}

// Classe de m�morisation static
void TestMemStatic()
{
	// Entre 2 ex�cutions val conserve sa valeur
	static int val = 100;	// par d�faut initialis� � 0
	val++;
	cout << val << endl;
}

// Classe de m�morisation extern
void TestMemExtern()
{
	cout << testVarExten << endl;
}

std::string* testRetour(int n)
{
	/*string tmp = string(n, 'a');	// variable locale
	return &tmp;*/
	string* tmp = new string(n, 'a');
	return tmp;
}

// Un caract�re sur deux
// �crire une fonction UnSurDEux qui programme qui affiche un caract�re sur deux (le premier est affich�)
void unSurDeux(const string& str)
{
	for (int i = 0; i < str.size(); i++)
	{
		if (i % 2 == 0) {
			cout << str.at(i);
		}
	}
	cout << endl;
}


// Exercice Inversion de chaine
// �crire la fonction inverser qui prend en param�tre une chaine et qui retourne la chaine avec les caract�res invers�s
string inverser(const string& str)
{
	string tmp = "";
	for (auto it = str.rbegin(); it != str.rend(); it++)
	{
		tmp.push_back(*it);
	}
	return tmp;
}

// Exercice Palindrome
// �crire une m�thode Palindrome qui accepte en param�tre une chaine et qui retourne un bool�en pour indiquer si c'est palindrome
bool palindrome(const std::string& str)
{
	return str == inverser(str);
}

// Nombre d'arguments variable
// en C et c++98
double moyenne(int nbArgs, ...)
{
	double somme = 0;
	va_list list;
	va_start(list, nbArgs);
	for (int i = 0; i < nbArgs; i++)
	{
		somme += va_arg(list, int);
	}
	va_end(list);	// nettoyage des arguments
	return nbArgs == 0 ? 0.0 : somme / nbArgs;
}